<?php

use Illuminate\Support\Facades\Route;
use Pear\TokenSimple\Http\Controllers\TemporaryPersonalTokenController;

Route::get('mipaquete/main', [TemporaryPersonalTokenController::class, 'index']);

Route::post('verificate-token', [TemporaryPersonalTokenController::class, 'verificateToken']);
