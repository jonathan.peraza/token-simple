<?php

namespace Pear\TokenSimple;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TemporaryPersonalToken extends Model
{
    use HasFactory;

    protected $fillable = [
        'provider',
        'tokenID',
        'perosonal_token',
        'expired_at'
    ];
}
