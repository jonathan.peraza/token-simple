<?php

namespace Pear\TokenSimple\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pear\TokenSimple\TemporaryPersonalToken;

class TemporaryPersonalTokenController extends Controller
{

    public function index(Request $request)
    {
        return "Hola mundo";
    }

    public function verificateToken(Request $request)
    {
        $request->validate([
            'token' => 'required'
        ]);

        $token = TemporaryPersonalToken::where('perosonal_token', $request->token)->first();

        if (!is_null($token)) {
            if ($token->expired_at >= now()) {
                return response()->json([
                    'message' => 'Token valid'
                ]);
            }
        }
        return response()->json([
            'message' => 'Token Invalid'
        ]);
    }
}
