<?php

namespace Pear\TokenSimple;

use Illuminate\Support\ServiceProvider;

class TokenSimpleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->app->make('Pear\TokenSimple\Http\Controllers\TemporaryPersonalTokenController');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if (app()->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../database/migrations' => database_path('migrations'),
            ], 'token-simple-migration');
        }
    }
}
